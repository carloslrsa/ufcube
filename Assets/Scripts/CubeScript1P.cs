﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CubeScript1P : MonoBehaviour {


	public string horizontalAxe = "1PHorizontal";
	public string verticalAxe = "1PVertical";
	public string dashAxe = "1PDash";
	[Header("Player Settings")]

	public float dashForce = 15f;
	public float dashTime = .5f;
	public float timeToDashAgain = 1f;
	public float airAceleration = 20f;
	public float groundAceleration = 20f;
	public Vector2 maxVelocity = new Vector2 (8f, 8f);
	public float jumpImpulse = 5f;

	private Rigidbody2D rb2d;
	private CapsuleCollider2D capsuleCol;
	public bool isGrounded = false;

	[Header("Environment Settings")]

	public LayerMask groundMask;
	public bool isDashing = false;
	public bool canDash = true;
	public bool isStunned = false;
	public float stunTime = 0f;
	public float hitForce = 2f;

	void Start () {
		//groundMask.value = 256;
		rb2d = GetComponent<Rigidbody2D> ();
		capsuleCol = GetComponent<CapsuleCollider2D> ();
	}


	void Update () {
		IsGrounded ();
		//WallJump ();

	}

	void FixedUpdate () {
		if (!isStunned) {
			Flip ();
			Jump ();
			if (!isDashing)
				Move ();
		} else {
			float aceleration = isGrounded ? groundAceleration : airAceleration;
			float compX = rb2d.velocity.x;
			if(compX != 0){
				compX = Mathf.Lerp (compX, 0, Time.deltaTime * aceleration);
				if (Mathf.Abs(compX) < .01f) {
					compX = 0;
				}
			}
			rb2d.velocity = new Vector2 (compX, rb2d.velocity.y);
		}
	}

	void IsGrounded () {
		Vector2 rayOriginLeft = new Vector2 (transform.position.x + capsuleCol.size.x / 2f - .1f, transform.position.y - capsuleCol.size.y - .05f);
		Vector2 rayOriginRight = new Vector2 (transform.position.x - (capsuleCol.size.x / 2f - .1f), transform.position.y - capsuleCol.size.y - .05f);
		RaycastHit2D hit1 = Physics2D.Raycast (rayOriginLeft, Vector2.down, 0.02f,groundMask);
		RaycastHit2D hit2 = Physics2D.Raycast (rayOriginRight, Vector2.down, 0.02f, groundMask);
		if (hit1 || hit2) {
			isGrounded = true;
		} else {
			isGrounded = false;
		}
	}

	void Move () {
		float inputH;
		bool isPressingH;
		inputH = Input.GetAxis (horizontalAxe);
		isPressingH = Input.GetButton(horizontalAxe);

		float aceleration = isGrounded ? groundAceleration : airAceleration;
		float compX = rb2d.velocity.x;

		if (isGrounded) {
			if (inputH > 0) {
				if (isPressingH) {
					inputH = 1;
				} else {
					inputH = 0;
				}
			} else if (inputH < 0) {
				if (isPressingH) {
					inputH = -1;
				} else {
					inputH = 0;
				}
			} else {
				inputH = 0;
			}
		}


		if (inputH != 0) {
			if (Mathf.Sign (compX) != Mathf.Sign (inputH) && !isGrounded) {
				aceleration *= 2f;
			}
			compX = Mathf.Clamp (compX + (Time.deltaTime * aceleration * inputH),-maxVelocity.x , maxVelocity.x); 
		} else {
			if(compX != 0){
				compX = Mathf.Lerp (compX, 0, Time.deltaTime * aceleration);
				if (Mathf.Abs(compX) < .01f) {
					compX = 0;
				}
			}
		}
		rb2d.velocity = new Vector2 (compX, rb2d.velocity.y);
		if (inputH != 0) {
			Dash (inputH);
		}

	}

	void Dash (float inputH) {
		if (canDash) {
			bool dashInput;
			dashInput = Input.GetButtonDown(dashAxe);
			if (dashInput) {
				rb2d.velocity = new Vector2 (dashForce * inputH, 0);
				StartCoroutine (IsTimeToDash (inputH));
			}
		}
	}

	IEnumerator IsTimeToDash (float inputH) {
		if (dashTime < timeToDashAgain) {
			canDash = false;
			isDashing = true;
			yield return new WaitForSeconds (dashTime);
			isDashing = false;
			yield return new WaitForSeconds (timeToDashAgain - dashTime);
			canDash = true;
		} else {
			Debug.Log ("Dash time must be less than time to dash again");
		}
	}

	void Jump () {
		if (isGrounded && Input.GetButton (verticalAxe) && Input.GetAxis (verticalAxe) > 0) {
			rb2d.velocity = new Vector2 (rb2d.velocity.x, maxVelocity.y);
		}
	}

	void WallJump () {
		Vector2 rayOriginLeft = new Vector2 (transform.position.x - .7f, transform.position.y);
		Vector2 rayOriginRight = new Vector2 (transform.position.x + .7f, transform.position.y);
		RaycastHit2D hitLeft = Physics2D.Raycast (rayOriginLeft, Vector2.left, 0.05f, groundMask);
		RaycastHit2D hitRight = Physics2D.Raycast (rayOriginRight, Vector2.right, 0.05f, groundMask);

		if (hitLeft) {
			if (Input.GetButton (verticalAxe) && Input.GetAxis (verticalAxe) > 0){
				rb2d.velocity = new Vector2 (jumpImpulse, maxVelocity.y);
			}
		}

		if (hitRight) {
			if (Input.GetButton (verticalAxe) && Input.GetAxis (verticalAxe) > 0) {
				rb2d.velocity = new Vector2 (-jumpImpulse, maxVelocity.y);
			}
		}

	}


	void Flip () {
		if (rb2d.velocity.x > 0 && transform.localScale.x < 0) {
			transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}
		if (rb2d.velocity.x < 0 && transform.localScale.x > 0) {
			transform.localScale = new Vector3 (-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		}
	}

	void OnCollisionEnter2D (Collision2D col) {
		if (col.contacts [0].normal.y < .1f && isDashing/* && !isStunned*/) {
			isDashing = false;
			GameObject contactObject = col.collider.gameObject;
			if (contactObject.tag.Equals ("Player")) {
				CubeScript1P oponentController = contactObject.GetComponent<CubeScript1P> ();
				oponentController.StunPlayer (2f, new Vector2 (hitForce * col.contacts[0].normal.x * -1, hitForce));
			}
			//rb2d.velocity = new Vector2 (hitForce * col.contacts[0].normal.x, hitForce);
		}
	}

	public void StunPlayer(float time){
		stunTime = time;
		rb2d.velocity = Vector2.zero;
		StartCoroutine (StunPlayerRoutine ());
	}

	public void StunPlayer(float time, Vector2 force){
		StunPlayer (time);
		rb2d.velocity = force;
	}

	IEnumerator StunPlayerRoutine(){
		isStunned = true;
		yield return new WaitForSeconds (stunTime);
		isStunned = false;
	}

	void OnTriggerEnter2D (Collider2D col) {
	}
}
